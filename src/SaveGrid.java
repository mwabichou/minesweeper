import java.io.*;
import java.io.PrintWriter;

class SaveGrid {
    public SaveGrid() {
    }

    public void save(Grid grid, String path) {
        try (PrintWriter out = new PrintWriter(path)) {
            out.println(grid.getRows());
            out.println(grid.getCols());
            out.println(grid.getMineRatio());
            for (int y = 0; y < grid.getRows(); y++) {
                for (int x = 0; x < grid.getCols(); x++) {
                    if (grid.isUncover(x, y)) {
                        out.print("U");
                    } else if (grid.isFlag(x, y)) {
                        if (grid.isBomb(x, y)) {
                            out.print("X");
                        } else {
                            out.print("F");
                        }
                    } else if (grid.isBomb(x, y)) {
                        out.print("B");
                    } else {
                        out.print("C");
                    }
                }
                out.println();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void save(File file, Grid grid) {
        try (PrintWriter out = new PrintWriter(file)) {
            out.println(grid.getRows());
            out.println(grid.getCols());
            out.println(grid.getMineRatio());
            for (int y = 0; y < grid.getRows(); y++) {
                for (int x = 0; x < grid.getCols(); x++) {
                    if (grid.isUncover(x, y)) {
                        out.print("U");
                    } else if (grid.isFlag(x, y)) {
                        if (grid.isBomb(x, y)) {
                            out.print("X");
                        } else {
                            out.print("F");
                        }
                    } else if (grid.isBomb(x, y)) {
                        out.print("B");
                    } else {
                        out.print("C");
                    }
                }
                out.println();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

}
