import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.io.File;

import javax.swing.*;
import javax.swing.plaf.FontUIResource;

class MenuUI extends JFrame {
    private static final long serialVersionUID = 1L;
    private ImageIcon playIcon = new ImageIcon("src/img/play.png");
    private ImageIcon optionsIcon = new ImageIcon("src/img/options.png");
    private ImageIcon exitIcon = new ImageIcon("src/img/exit.png");
    private ImageIcon terminalIcon = new ImageIcon("src/img/terminal.png");
    private ImageIcon logoIcon = new ImageIcon("src/img/logo_menu.png");
    private ImageIcon loadIcon = new ImageIcon("src/img/load.png");
    private ImageIcon backgroundIcon = new ImageIcon("src/img/background.png");
    private int rows = 10;
    private int cols = 10;
    private float mineRatio = 0.2f;
    private int hgap = 5;
    private int vgap = 5;
    private int height = 888;
    private int width = 1250;
    private int mode = 0;

    public MenuUI() {
        super("Minesweeper | Menu");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setMinimumSize(new Dimension(800, 800));
        setLocationRelativeTo(null);
        setResizable(false);
        setIconImage(logoIcon.getImage());
        setVisible(true);
        this.setContentPane(new JPanel() {
            @Override
            public void paintComponent(Graphics g) {
                super.paintComponent(g);
                g.drawImage(backgroundIcon.getImage(), 0, 0, 1300, 800, null);
            }
        });
        setLayout(null);
        JLabel logo = new JLabel();
        logo.setIcon(logoIcon);
        logo.setBounds(200, 45, 400, 120);
        add(logo);

        JButton new_game = new JButton("Play Game");
        new_game.setFont(new Font("Helvetica", Font.BOLD, 35));
        new_game.setOpaque(true);
        new_game.setBackground(new Color(0, 0, 0, 50));
        new_game.setIcon(playIcon);
        new_game.setHorizontalTextPosition(SwingConstants.LEFT);
        new_game.setForeground(Color.white);
        new_game.setBorder(BorderFactory.createLineBorder(Color.white));
        new_game.setRolloverEnabled(true);
        new_game.setAlignmentX(Component.CENTER_ALIGNMENT);
        new_game.setAlignmentY(Component.CENTER_ALIGNMENT);
        new_game.setBounds(250, 200, 300, 80);

        JButton loadBtn = new JButton("Load Game");
        loadBtn.setFont(new Font("Helvetica", Font.BOLD, 35));
        loadBtn.setOpaque(true);
        loadBtn.setBackground(new Color(0, 0, 0, 50));
        loadBtn.setIcon(loadIcon);
        loadBtn.setHorizontalTextPosition(SwingConstants.LEFT);
        loadBtn.setForeground(Color.white);
        loadBtn.setBorder(BorderFactory.createLineBorder(Color.white));
        loadBtn.setRolloverEnabled(true);
        loadBtn.setAlignmentX(Component.CENTER_ALIGNMENT);
        loadBtn.setAlignmentY(Component.CENTER_ALIGNMENT);
        loadBtn.setBounds(250, 320, 300, 80);
        JButton optionsBtn = new JButton("Options");
        optionsBtn.setFont(new Font("Helvetica", Font.BOLD, 35));
        optionsBtn.setOpaque(true);
        optionsBtn.setForeground(Color.white);
        optionsBtn.setIcon(optionsIcon);
        optionsBtn.setHorizontalTextPosition(SwingConstants.LEFT);
        optionsBtn.setBackground(new Color(0, 0, 0, 50));
        optionsBtn.setBorder(BorderFactory.createLineBorder(Color.white));
        optionsBtn.setRolloverEnabled(true);
        optionsBtn.setAlignmentX(Component.CENTER_ALIGNMENT);
        optionsBtn.setAlignmentY(Component.CENTER_ALIGNMENT);
        optionsBtn.setBounds(250, 440, 300, 80);
        JButton terminal_mode = new JButton("TerminalUI");
        terminal_mode.setFont(new Font("Helvetica", Font.BOLD, 35));
        terminal_mode.setOpaque(true);
        terminal_mode.setIcon(terminalIcon);
        terminal_mode.setHorizontalTextPosition(SwingConstants.LEFT);
        terminal_mode.setBackground(new Color(0, 0, 0, 50));
        terminal_mode.setForeground(Color.white);
        terminal_mode.setBorder(BorderFactory.createLineBorder(Color.white));
        terminal_mode.setRolloverEnabled(true);
        terminal_mode.setAlignmentX(Component.CENTER_ALIGNMENT);
        terminal_mode.setAlignmentY(Component.CENTER_ALIGNMENT);
        terminal_mode.setBounds(250, 560, 300, 80);

        JButton exitBtn = new JButton("Exit");
        exitBtn.setFont(new Font("Helvetica", Font.BOLD, 35));
        exitBtn.setOpaque(true);
        exitBtn.setBackground(new Color(0, 0, 0, 50));
        exitBtn.setForeground(Color.white);
        exitBtn.setIcon(exitIcon);
        exitBtn.setHorizontalTextPosition(SwingConstants.LEFT);
        exitBtn.setBorder(BorderFactory.createLineBorder(Color.white));
        exitBtn.setRolloverEnabled(true);
        exitBtn.setAlignmentX(Component.CENTER_ALIGNMENT);
        exitBtn.setAlignmentY(Component.CENTER_ALIGNMENT);
        exitBtn.setBounds(250, 680, 300, 80);

        exitBtn.addActionListener(e -> System.exit(0));
        loadBtn.addActionListener(e -> loadFile());
        new_game.addActionListener(e -> new_game());
        optionsBtn.addActionListener(e -> {
            mode = options();
            setMode(mode);
        });
        terminal_mode.addActionListener(e -> terminal_mode());
        add(new_game);
        add(loadBtn);
        add(terminal_mode);
        add(optionsBtn);
        add(exitBtn);
        pack();

    }

    private void loadFile() {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Minesweeper | Load");
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fileChooser.setCurrentDirectory(new File("src/save"));
        int userSelection = fileChooser.showOpenDialog(this);
        if (userSelection == JFileChooser.APPROVE_OPTION) {
            LoadGrid loader = new LoadGrid();
            Grid game = loader.load(fileChooser.getSelectedFile());
            new Window(game, width, height);
        }
    }

    private void new_game() {

        Grid game = new Grid(rows, cols, mineRatio, new GridLayout(rows, cols, hgap, vgap));
        new Window(game, width, height);
    }

    private static int options() {
        UIManager.put("OptionPane.messageFont", new FontUIResource(new Font("Helvetica", Font.BOLD, 20)));
        UIManager.put("OptionPane.buttonFont", new FontUIResource(new Font("Helvetica", Font.BOLD, 20)));
        UIManager.put("OptionPane.minimumSize", new Dimension(500, 200));
        UIManager.put("OptionPane.background", new Color(172, 133, 174));
        UIManager.put("Panel.background", new Color(172, 133, 174));
        UIManager.put("OptionPane.messageForeground", Color.white);
        UIManager.put("OptionPane.messageAlignment", SwingConstants.CENTER);
        int mode = JOptionPane.showOptionDialog(null, "Choose a mode :", "Minesweeper | Options",
                JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null, new String[] { "Easy", "Medium", "Hard" },
                "Easy");
        return mode;

    }

    private void terminal_mode() {
        dispose();
        new TerminalMode().play();
    }

    private void setMode(int mode) {
        switch (mode) {
            case 0:
                rows = 10;
                cols = 10;
                mineRatio = 0.1f;
                height = 888;
                width = 1250;
                break;
            case 1:
                rows = 16;
                cols = 16;
                mineRatio = 0.15f;
                height = 888;
                width = 1250;
                break;
            case 2:
                rows = 16;
                cols = 30;
                mineRatio = 0.2f;
                height = 888;
                hgap = 2;
                vgap = 2;
                Dimension screensize = Toolkit.getDefaultToolkit().getScreenSize();
                width = (int) screensize.getWidth();
                break;
        }
    }
}
