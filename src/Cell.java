
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.*;
import java.util.EnumMap;
import javax.swing.*;

class Cell {
    private boolean isBomb;
    private boolean isUncover;
    private boolean isFlag;
    EnumMap<Direction, Cell> neighbors;
    private ImageIcon Bombicon = new ImageIcon("src/img/bomb.png");
    private ImageIcon Flagicon = new ImageIcon("src/img/flag.png");
    JButton button;

    public Cell() {
        this.isBomb = false;
        this.isUncover = false;
        this.neighbors = new EnumMap<>(Direction.class);
    }

    public void setButton() {
        this.button = new JButton();
        button.setFont(new Font("Helvetica", Font.BOLD, 30));
        button.setOpaque(true);
        button.setBorder(BorderFactory.createLineBorder(Color.darkGray));
        button.setBackground(Color.lightGray);
        button.setRolloverEnabled(true);
        button.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (SwingUtilities.isRightMouseButton(e)) {
                    if (isUncover) {
                        return;
                    }
                    button.setIcon(null);
                    toggleFlag();
                    updateCell();
                } else {
                    uncover();
                    updateCell();
                }
            }

            public void mouseEntered(MouseEvent e) {
                if (!isUncover) {
                    button.setBackground(Color.white);
                }

            }

            public void mouseExited(MouseEvent e) {
                if (!isUncover) {
                    button.setBackground(Color.lightGray);
                }
            }

            public void mousePressed(MouseEvent e) {
            }

            public void mouseReleased(MouseEvent e) {
            }
        });
    }

    public void uncover() {
        if (isUncover || isFlag) {
            return;
        }
        if (button != null) {
            button.setBackground(Color.white);
        }
        isUncover = true;
        if (isBomb) {
            return;
        }
        if (countAdjacentMines() == 0) {
            for (Cell neighbor : neighbors.values()) {
                neighbor.uncover();
            }
        } else {
            if (button != null) {
                updateCell();
            }
        }
    }

    private int countAdjacentMines() {
        int count = 0;
        for (Cell neighbor : neighbors.values()) {
            if (neighbor.isBomb) {
                count++;
            }
        }
        return count;
    }

    public String printCell() {

        if (isFlag) {
            return "\uD83D\uDEA9";
        }
        if (!isUncover) {
            return "\uD83D\uDFE6";
        }
        if (isBomb) {
            return "\uD83D\uDCA3";
        }
        int count = countAdjacentMines();
        if (count == 0) {
            return "\u2B1C";

        }
        return " " + intColor(32 + count) + count + "\u001B[0m";
    }

    public String drawCell() {
        if (isUncover) {
            button.setBackground(Color.white);
        }
        if (isFlag) {
            // size of the icon depends on the button size
            button.setIcon(new ImageIcon(Flagicon.getImage().getScaledInstance(30, 30, Image.SCALE_SMOOTH)));
            return "";
        }
        if (!isUncover) {
            return "";
        }
        if (isBomb) {
            button.setBackground(Color.RED);
            button.setIcon(new ImageIcon(Bombicon.getImage().getScaledInstance(60, 60, Image.SCALE_SMOOTH)));
            return "";
        }
        int count = countAdjacentMines();
        if (count == 0) {
            return " ";
        }
        setNumberColor(count);
        return String.valueOf(count);
    }

    public void updateCell() {
        button.setText(drawCell());
    }

    private void setNumberColor(int count) {
        switch (count) {
            case 1:
                button.setForeground(new Color(9, 188, 138));
                break;
            case 2:
                button.setForeground(new Color(55, 114, 255));
                break;
            case 3:
                button.setForeground(new Color(221, 28, 26));
                break;
            case 4:
                button.setForeground(new Color(102, 44, 145));
                break;
            case 5:
                button.setForeground(new Color(4, 231, 98));
                break;
            case 6:
                button.setForeground(new Color(68, 255, 210));
                break;
            case 7:
                button.setForeground(new Color(227, 101, 193));
                break;
            case 8:
                button.setForeground(new Color(248, 243, 43));
                break;
            default:
                break;
        }
    }

    private String intColor(int color) {
        return "\u001B[" + color + "m";
    }

    public boolean isBomb() {
        return isBomb;
    }

    public void setBomb(boolean bomb) {
        isBomb = bomb;
    }

    public void toggleFlag() {
        isFlag = !isFlag;
    }

    public boolean isFlag() {
        return isFlag;
    }

    public boolean isUncover() {
        return isUncover;
    }

    public void setUncover(boolean uncover) {
        isUncover = uncover;
    }

    public void setFlag(boolean flag) {
        isFlag = flag;
    }
}