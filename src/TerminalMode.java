import java.io.IOException;
import java.util.*;

class TerminalMode {

    Grid grid;
    int posX;
    int posY;
    private boolean isOver;
    private LoadGrid loader;
    private SaveGrid saver;

    public TerminalMode() {
        this.isOver = false;
    }

    private void mainMenu() {
        new MenuUI();
    }

    public void play() {
        try (Scanner in = new Scanner(System.in)) {
            System.out.println("Load or New game? (l/n)");
            String input = in.next();
            if (input.equals("l")) {
                System.out.println("Enter path to save file");
                String path = in.next();
                System.out.println(path);
                grid = loader.load(path);
            } else if (input.equals("n")) {
                System.out.println("Enter width");
                int width = in.nextInt();
                System.out.println("Enter height");
                int height = in.nextInt();
                System.out.println("Enter mine ratio");
                float mineRatio = in.nextFloat();
                grid = new Grid(width, height, mineRatio);
            }
            while (!isOver) {
                grid.printGrid();
                System.out.println("u: uncover, f: flag, S: save, q: quit, m: main menu");
                String inputInGame = in.next();
                if (inputInGame.equals("u")) {
                    System.out.println("Enter x and y");
                    int x = in.nextInt();
                    int y = in.nextInt();
                    grid.Uncover(x, y);
                } else if (inputInGame.equals("f")) {
                    System.out.println("Enter x and y");
                    int x = in.nextInt();
                    int y = in.nextInt();
                    grid.setFlag(x, y);
                } else if (inputInGame.equals("s")) {
                    System.out.println("Enter path to save file");
                    String path = in.next();
                    saver.save(grid, path);
                } else if (inputInGame.equals("q")) {
                    isOver = true;
                } else if (inputInGame.equals("m")) {
                    mainMenu();
                }
                clearConsole();
                if (grid.checkWin()) {
                    grid.printGrid();
                    System.out.println("You won!");
                    isOver = true;
                } else if (grid.checkLoose()) {
                    grid.printGrid();
                    System.out.println("You lost!");
                    isOver = true;
                }
            }
        }
    }

    public static void clearConsole() {
        try {
            if (System.getProperty("os.name").contains("Windows")) {
                new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
            } else {
                System.out.print("\033\143");
            }
        } catch (IOException | InterruptedException ex) {
        }
    }
}
