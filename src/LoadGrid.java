import java.awt.GridLayout;
import java.io.File;
import java.util.Locale;
import java.util.Scanner;

class LoadGrid {

    public LoadGrid() {
    }

    public Grid load(String path) {
        try {
            File saveFile = new File(path);
            try (Scanner in = new Scanner(saveFile).useLocale(Locale.forLanguageTag("fr-FR"))) {
                int height = in.nextInt();
                int width = in.nextInt();
                String mineRatioStr = in.next().replace(',', '.');
                float mineRatio = Float.parseFloat(mineRatioStr);
                Grid grid = new Grid(height, width, mineRatio, new GridLayout(height, width, 5, 5));
                for (int y = 0; y < height; y++) {
                    String line = in.next();
                    for (int x = 0; x < width; x++) {
                        char c = line.charAt(x);
                        if (c == 'U') {
                            grid.unSetBomb(x, y);
                            grid.uncover(x, y);
                        } else if (c == 'F') {
                            grid.setFlag(x, y);
                        } else if (c == 'X') {
                            grid.setBomb(x, y);
                            grid.setFlag(x, y);
                        } else if (c == 'B') {
                            grid.setBomb(x, y);
                        } else {
                            grid.unSetBomb(x, y);
                        }
                    }
                }
                in.close();
                grid.setCellNeighbors();
                return grid;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Error while loading the file");
        }
    }

    public Grid load(File saveFile) {
        try {
            try (Scanner in = new Scanner(saveFile).useLocale(Locale.forLanguageTag("fr-FR"))) {
                int height = in.nextInt();
                int width = in.nextInt();
                String mineRatioStr = in.next().replace(',', '.');
                float mineRatio = Float.parseFloat(mineRatioStr);
                Grid grid = new Grid(height, width, mineRatio, new GridLayout(height, width, 5, 5));
                for (int y = 0; y < height; y++) {
                    String line = in.next();
                    for (int x = 0; x < width; x++) {
                        char c = line.charAt(x);
                        if (c == 'U') {
                            grid.unSetBomb(x, y);
                            grid.uncover(x, y);
                        } else if (c == 'F') {
                            grid.setFlag(x, y);
                        } else if (c == 'X') {
                            grid.setBomb(x, y);
                            grid.setFlag(x, y);
                        } else if (c == 'B') {
                            grid.setBomb(x, y);
                        } else {
                            grid.unSetBomb(x, y);
                        }
                    }
                }
                in.close();
                grid.setCellNeighbors();
                return grid;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Error while loading the file");
        }
    }

}
