import java.awt.*;
import java.util.*;
import javax.swing.*;

public class Grid {
    private final int rows;
    private final int cols;
    private final float minesRatio;
    private final Cell[][] gridOfCells;
    JPanel panel;

    private final int totalMines;

    public Grid(int rows, int cols, float mineRatio) {
        if (cols < 1 || rows < 1 || mineRatio < 0 || mineRatio > 1) {
            throw new IllegalArgumentException("Invalid grid parameters");
        }
        this.cols = cols;
        this.rows = rows;
        this.minesRatio = mineRatio;
        this.totalMines = calcTotalMines(minesRatio);
        this.gridOfCells = new Cell[rows][cols];
        initGrid();
    }

    public Grid(int rows, int cols, float mineRatio, GridLayout g) {
        if (cols < 1 || rows < 1 || mineRatio < 0 || mineRatio > 1) {
            throw new IllegalArgumentException("Invalid grid parameters");
        }
        this.panel = new JPanel(g);
        this.cols = cols;
        this.rows = rows;
        this.minesRatio = mineRatio;
        this.totalMines = calcTotalMines(minesRatio);
        this.gridOfCells = new Cell[rows][cols];
        initGrid();
        addCellsToPanel();
    }

    private void initGrid() {
        for (int x = 0; x < rows; x++) {
            for (int y = 0; y < cols; y++) {
                gridOfCells[x][y] = new Cell();
            }
        }
        putMines();
        setCellNeighbors();
    }

    private void putMines() {
        Random rand = new Random();
        int nbMines = 0;
        while (nbMines < totalMines) {
            int x = rand.nextInt(rows);
            int y = rand.nextInt(cols);
            if (!gridOfCells[x][y].isBomb()) {
                gridOfCells[x][y].setBomb(true);
                nbMines++;
            }
        }
    }

    public void setCellNeighbors() {
        for (int x = 0; x < rows; x++) {
            for (int y = 0; y < cols; y++) {
                Cell cell = gridOfCells[x][y];
                if (x > 0) {
                    cell.neighbors.put(Direction.TOP, gridOfCells[x - 1][y]);
                    if (y > 0) {
                        cell.neighbors.put(Direction.TOP_LEFT, gridOfCells[x - 1][y - 1]);
                    }
                    if (y < cols - 1) {
                        cell.neighbors.put(Direction.TOP_RIGHT, gridOfCells[x - 1][y + 1]);
                    }
                }
                if (x < rows - 1) {
                    cell.neighbors.put(Direction.BOTTOM, gridOfCells[x + 1][y]);
                    if (y > 0) {
                        cell.neighbors.put(Direction.BOTTOM_LEFT, gridOfCells[x + 1][y - 1]);
                    }
                    if (y < cols - 1) {
                        cell.neighbors.put(Direction.BOTTOM_RIGHT, gridOfCells[x + 1][y + 1]);
                    }
                }
                if (y > 0) {
                    cell.neighbors.put(Direction.LEFT, gridOfCells[x][y - 1]);
                }
                if (y < cols - 1) {
                    cell.neighbors.put(Direction.RIGHT, gridOfCells[x][y + 1]);
                }
            }
        }
    }

    public int getCols() {
        return cols;
    }

    public int getRows() {
        return rows;
    }

    public float getMineRatio() {
        return minesRatio;
    }

    public int getTotalMines() {
        return totalMines;
    }

    private int calcTotalMines(float mineRatio) {
        return (int) (cols * rows * mineRatio);
    }

    public void printGrid() {
        System.out.print("  ");
        for (int x = 0; x < cols; x++) {
            System.out.print(x + "|");
        }
        System.out.println();
        for (int x = 0; x < rows; x++) {
            System.out.print(x + "|");
            for (int y = 0; y < cols; y++) {
                System.out.print(gridOfCells[x][y].printCell());
            }
            System.out.println();
        }
    }

    public Cell getCell(int x, int y) {
        return gridOfCells[x][y];
    }

    private void addCellsToPanel() {
        for (int x = 0; x < rows; x++) {
            for (int y = 0; y < cols; y++) {
                gridOfCells[x][y].setButton();
                panel.add(gridOfCells[x][y].button);
            }
        }
    }

    public int getTotalUncovered() {
        int totalUncovered = 0;
        for (int x = 0; x < rows; x++) {
            for (int y = 0; y < cols; y++) {
                if (gridOfCells[x][y].isUncover()) {
                    totalUncovered++;
                }
            }
        }
        return totalUncovered;
    }

    public int getTotalFlaged() {
        int totalFlaged = 0;
        for (int x = 0; x < rows; x++) {
            for (int y = 0; y < cols; y++) {
                if (gridOfCells[x][y].isFlag()) {
                    totalFlaged++;
                }
            }
        }
        return totalFlaged;
    }

    public int getTotalBombFlaged() {
        int totalBombFlaged = 0;
        for (int x = 0; x < rows; x++) {
            for (int y = 0; y < cols; y++) {
                if (gridOfCells[x][y].isFlag() && gridOfCells[x][y].isBomb()) {
                    totalBombFlaged++;
                }
            }
        }
        return totalBombFlaged;
    }

    private void uncoverAllBombs() {
        for (int x = 0; x < rows; x++) {
            for (int y = 0; y < cols; y++) {
                if (gridOfCells[x][y].isBomb()) {
                    gridOfCells[x][y].setFlag(false);
                    gridOfCells[x][y].setUncover(true);
                    if (panel != null) {
                        gridOfCells[x][y].updateCell();
                    }
                }
            }
        }
    }

    public boolean checkLoose() {
        for (int x = 0; x < rows; x++) {
            for (int y = 0; y < cols; y++) {
                if (gridOfCells[x][y].isBomb() && gridOfCells[x][y].isUncover()) {
                    uncoverAllBombs();
                    return true;
                }
            }
        }
        return false;
    }

    public boolean checkWin() {
        if (getTotalFlaged() == getTotalBombFlaged() && getTotalUncovered() == cols * rows - totalMines) {
            return true;
        }
        return false;
    }

    public void reset() {
        panel.removeAll();
        for (int x = 0; x < rows; x++) {
            for (int y = 0; y < cols; y++) {
                gridOfCells[x][y] = new Cell();
            }
        }
        putMines();
        setCellNeighbors();
        addCellsToPanel();
    }

    public boolean isUncover(int x, int y) {
        return gridOfCells[x][y].isUncover();
    }

    public boolean isFlag(int x, int y) {
        return gridOfCells[x][y].isFlag();
    }

    public boolean isBomb(int x, int y) {
        return gridOfCells[x][y].isBomb();
    }

    public void uncover(int x, int y) {
        gridOfCells[x][y].setUncover(true);
    }

    public void setFlag(int x, int y) {
        gridOfCells[x][y].setFlag(true);
    }

    public void setBomb(int x, int y) {
        gridOfCells[x][y].setBomb(true);
    }

    public void unSetBomb(int x, int y) {
        gridOfCells[x][y].setBomb(false);
    }

    public void Uncover(int x, int y) {
        gridOfCells[x][y].uncover();
    }

    public void updateView() {
        for (int x = 0; x < rows; x++) {
            for (int y = 0; y < cols; y++) {
                gridOfCells[x][y].updateCell();
            }
        }
    }
}
