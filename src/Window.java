
import java.awt.*;
import javax.swing.*;
import javax.swing.border.LineBorder;

import java.awt.event.*;
import java.io.File;

public class Window extends JFrame {
    private static final long serialVersionUID = 1L;
    private int gamesPlayed = 0;
    private int gamesWon = 0;
    private int gamesLost = 0;
    private int gamesAborted = 0;

    public Window(Grid grid, int width, int height) {
        super("Minesweeper | Game");
        grid.updateView();
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setSize(screenSize.width, screenSize.height);

        setMinimumSize(new Dimension(width, height));
        setLocationRelativeTo(null);
        setResizable(true);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        JPanel topPanel = new JPanel();
        JPanel rightPanel = new JPanel();
        rightPanel.setLayout(null);
        JPanel leftPanel = new JPanel();
        Grid game = grid;
        JPanel bottomPanel = new JPanel();
        JLabel flagLabel = createInfoLabel("src/img/flag.png");
        flagLabel.setBounds(30, 30, 120, 120);
        rightPanel.add(flagLabel);
        JLabel sizeLabel = createInfoLabel("src/img/size.png");
        sizeLabel.setBounds(160, 30, 120, 120);
        rightPanel.add(sizeLabel);
        // logo.setHorizontalAlignment(JLabel.CENTER);
        // logo.setVerticalAlignment(JLabel.CENTER);
        JLabel checkLabel = createInfoLabel("src/img/check.png");
        checkLabel.setBounds(30, 160, 120, 120);
        rightPanel.add(checkLabel);
        JLabel clockLabel = createInfoLabel("src/img/clock.png");
        clockLabel.setBounds(160, 160, 120, 120);
        rightPanel.add(clockLabel);
        JLabel logo = createInfoLabel("src/img/logo2.png");
        logo.setBounds(30, 300, 250, 120);
        rightPanel.add(logo);
        JLabel gameInfo = createInfoLabel("");
        gameInfo.setBounds(30, 450, 250, 120);
        gameInfo.setBackground(Color.white);
        gameInfo.setFont(new Font("Helvetica", Font.BOLD, 20));
        gameInfo.setText(sessionInfo());
        rightPanel.add(gameInfo);

        JButton saveButton = new JButton("Save");
        saveButton.setFont(new Font("Helvetica", Font.BOLD, 30));
        saveButton.setOpaque(true);
        saveButton.setBackground(Color.lightGray);
        saveButton.setBorder(BorderFactory.createLineBorder(Color.darkGray));
        saveButton.setRolloverEnabled(true);
        saveButton.setIcon(new ImageIcon("src/img/save.png"));
        saveButton.setHorizontalTextPosition(SwingConstants.RIGHT);
        saveButton.setBounds(45, 620, 220, 70);
        rightPanel.add(saveButton);

        JButton restartBtn = new JButton("Restart");
        ImageIcon restartIcon = new ImageIcon("src/restart.png");
        restartBtn.setIcon(restartIcon);
        restartBtn.setFont(new Font("Helvetica", Font.BOLD, 30));
        restartBtn.setOpaque(true);
        restartBtn.setBackground(Color.lightGray);
        restartBtn.setBorder(BorderFactory.createLineBorder(Color.darkGray));
        restartBtn.setRolloverEnabled(true);
        restartBtn.setIcon(new ImageIcon("src/img/restart.png"));
        restartBtn.setHorizontalTextPosition(SwingConstants.RIGHT);
        restartBtn.setBounds(45, 700, 220, 70);
        rightPanel.add(restartBtn);

        JButton QuitBtn = new JButton("Quit");
        QuitBtn.setFont(new Font("Helvetica", Font.BOLD, 30));
        QuitBtn.setOpaque(true);
        QuitBtn.setBackground(Color.lightGray);
        QuitBtn.setBorder(BorderFactory.createLineBorder(Color.darkGray));
        QuitBtn.setRolloverEnabled(true);
        QuitBtn.setIcon(new ImageIcon("src/img/quit.png"));
        QuitBtn.setHorizontalTextPosition(SwingConstants.RIGHT);
        QuitBtn.setBounds(45, 780, 220, 70);
        rightPanel.add(QuitBtn);

        topPanel.setBackground(Color.lightGray);
        rightPanel.setBackground(Color.darkGray);
        leftPanel.setBackground(new Color(2, 128, 144));
        game.panel.setBackground(Color.darkGray);
        bottomPanel.setBackground(new Color(122, 40, 203));
        topPanel.setPreferredSize(new java.awt.Dimension(100, 100));
        rightPanel.setPreferredSize(new java.awt.Dimension(300, 400));
        leftPanel.setPreferredSize(new java.awt.Dimension(0, 0));
        game.panel.setPreferredSize(new java.awt.Dimension(400, 400));
        bottomPanel.setPreferredSize(new java.awt.Dimension(0, 0));
        // add(topPanel, BorderLayout.NORTH);
        add(rightPanel, BorderLayout.EAST);
        add(leftPanel, BorderLayout.WEST);
        add(game.panel, BorderLayout.CENTER);
        pack();
        Timer timer = new Timer(1000, new ActionListener() {
            int time = 0;

            @Override
            public void actionPerformed(ActionEvent e) {
                time++;
                if (time < 10) {
                    clockLabel.setText("00:0" + time);
                } else if (time < 60) {
                    clockLabel.setText("00:" + time);
                } else if (time < 600) {
                    clockLabel.setText("0" + time / 60 + ":" + time % 60);
                } else {
                    clockLabel.setText(time / 60 + ":" + time % 60);
                }
                flagLabel.setText(game.getTotalFlaged() + "/" + game.getTotalMines());
                sizeLabel.setText(game.getRows() + "X" + game.getCols());
                checkLabel.setText(game.getTotalUncovered() + game.getTotalFlaged() + "/"
                        + game.getCols() * game.getRows());
                if (game.checkLoose()) {
                    lose();
                    time = 0;
                    gamesLost++;
                    gameInfo.setText(sessionInfo());
                    game.reset();
                }
                if (game.checkWin()) {
                    win();
                    time = 0;
                    gamesWon++;
                    gameInfo.setText(sessionInfo());
                    game.reset();
                }

            }
        });
        timer.start();
        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SaveGrid saver = new SaveGrid();
                if (game.getTotalUncovered() != 0) {
                    JFileChooser fileChooser = new JFileChooser();
                    fileChooser.setDialogTitle("MineSweeper | Save");
                    fileChooser.setCurrentDirectory(new File("src/save"));
                    int userSelection = fileChooser.showSaveDialog(null);
                    if (userSelection == JFileChooser.APPROVE_OPTION) {
                        File fileToSave = fileChooser.getSelectedFile();
                        saver.save(fileToSave, game);
                    }
                }
            }
        });
        restartBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (game.getTotalUncovered() != 0) {
                    game.reset();
                    flagLabel.setText(game.getTotalFlaged() + "/" + game.getTotalMines());
                    sizeLabel.setText(game.getRows() + "X" + game.getCols());
                    checkLabel.setText(game.getTotalUncovered() + game.getTotalFlaged() + "/"
                            + game.getCols() * game.getRows());
                    gamesAborted++;
                    gameInfo.setText(sessionInfo());
                    game.panel.repaint();
                }
            }
        });
        QuitBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        setVisible(true);
    }

    private static void lose() {
        UIManager.put("OptionPane.background", Color.LIGHT_GRAY);
        UIManager.put("Panel.background", Color.LIGHT_GRAY);
        // change icon of the message
        UIManager.put("OptionPane.informationIcon", new ImageIcon("src/img/bomb3.png"));
        UIManager.put("OptionPane.messageForeground", Color.red);
        JOptionPane.showMessageDialog(null, "You lost...");
    }

    private static void win() {
        UIManager.put("OptionPane.background", Color.lightGray);
        UIManager.put("Panel.background", Color.LIGHT_GRAY);
        UIManager.put("OptionPane.informationIcon", new ImageIcon("src/img/hero.png"));
        UIManager.put("OptionPane.messageForeground", new Color(0, 200, 128));
        JOptionPane.showMessageDialog(null, "You win...");
    }

    private JLabel createInfoLabel(String fileName) {
        JLabel label = new JLabel();
        ImageIcon icon = new ImageIcon(fileName);
        LineBorder border = new LineBorder(Color.black, 3, true);
        label.setIcon(icon);
        label.setFont(new Font("Helvetica", Font.BOLD, 25));
        label.setHorizontalTextPosition(JLabel.CENTER);
        label.setVerticalTextPosition(JLabel.BOTTOM);
        label.setVerticalAlignment(JLabel.CENTER);
        label.setHorizontalAlignment(JLabel.CENTER);
        label.setBorder(border);
        label.setForeground(Color.white);
        return label;
    }

    private String sessionInfo() {
        gamesPlayed = gamesWon + gamesLost + gamesAborted;
        String labelText = "<html>Session Info:<br>Games Played:" + " " + gamesPlayed + "<br>Games Won:" + " "
                + gamesWon + "<br>Games Lost:" + " " + gamesLost + "<br>Games Aborted:" + " " + gamesAborted
                + "</html>";
        return labelText;
    }
}